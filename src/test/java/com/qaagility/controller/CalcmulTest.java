package com.qaagility.controller;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public class CalcmulTest {

    @Test
    public void mul() {
        final Calcmul calcmul = new Calcmul();
        final int product = calcmul.mul();
        assertEquals("Multiplication is wrong", 18, product);
    }
}