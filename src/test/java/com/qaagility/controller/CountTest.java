package com.qaagility.controller;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public class CountTest {
    private Count count;

    @Before
    public void setUp() throws Exception {
        count = new Count();
    }

    @Test
    public void divide() {
        final int quotient = count.divide(6, 3);
        assertEquals("Division is wrong", 2, quotient);
    }

    @Test
    public void divideByZero() {
        final int quotient = count.divide(6, 0);
        assertEquals("Division by zero is wrong", Integer.MAX_VALUE, quotient);
    }
}