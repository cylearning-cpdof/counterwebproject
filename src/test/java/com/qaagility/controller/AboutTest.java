package com.qaagility.controller;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public class AboutTest {

    @Test
    public void desc() {
        final About about = new About();
        final String desc = about.desc();

        assertTrue("Description is wrong", desc.contains("This application was copied from somewhere"));
    }
}