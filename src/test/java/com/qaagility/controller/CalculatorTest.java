package com.qaagility.controller;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public class CalculatorTest {

    @Test
    public void add() {
        final Calculator calculator = new Calculator();
        final int sum = calculator.add();
        assertEquals("Addition is wrong", 9, sum);
    }
}